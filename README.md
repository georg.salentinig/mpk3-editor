# Akai MPK mini MK3 Editor
This is started as a simple command line tool for parsing and manipulating SysEx
messages to read out and set the programs in the
[Akai MPK mini MK3](https://www.akaipro.com/mpk-mini-mk3).

The software provided by Akai is Mac and Windows only. Wine works kind of, but
this is unsatisfactory.

Existing Editors are either not (yet) updated to support the new MK3 (
[here](https://github.com/gljubojevic/akai-mpk-mini-editor)
and
[here](https://github.com/PiOverFour/MPK-M2-editor)
) or have hard to meet dependencies (`rtmidi 4.0` :
[here](https://github.com/tsmetana/mpk3-settings)
).

This implementation is targeted to use existing tools (`amidi`, `aconnect`,
`aseqdump`) and only simple dependencies (`yaml-cpp`).

This currently can parse MPK mini MK3 SysEx Response from `stdin` and emits
human readable YAML files to the current working directory.
Modified YAML files can be programmed back to the MPK mini.
Warning : No error handling whatsoever!

## Compile
Clone with `--recursive` to include `yaml-cpp`.
in a build directory next to the cloned repository:
- `cmake -DCMAKE_BUILD_TYPE=Release ../mpk3-editor`
- `cmake --build .`

## Usage
- connect MPK mini MK3
- start monitor with `amidi -p virtual -d | ./mpk3_to_yaml`
-- Remark: received programs will be emitted as YAML files to this CWD.
- get MIDI address of MPK mini MK3 and monitor from `aconnect -l` and connect.
- get Hardware ID from `amidi -l` (usually `hw:1,0,0`)
- send request to MPK mini MK3 with e.g.
`amidi -p hw:1,0,0 -S 'F0 47 00 49 66 00 00 01 F7'`
(last byte before SysexEnd specifies program ID - here `01`)
- modify/create a YAML file
- disconnect from monitor
- program back with e.g.
```amidi -p hw:1,0,0 -S `./yaml_to_mpk3 ../configs/1_PGM_AVRSynth.yml` ```
