#include "mpk3_types.hpp"
#include "mpk3_yaml.hpp"
#include "yaml-cpp/yaml.h"
#include <iostream>


int main(int argc, char const ** argv) {
    mpk3::payload_t pgm;
    pgm = YAML::LoadFile(argv[1]).as<mpk3::payload_t>();
    std::cout << pgm;
    return 0;
}
