#include "mpk3_types.hpp"
#include "mpk3_yaml.hpp"
#include "yaml-cpp/yaml.h"
#include <iostream>
#include <fstream>
#include <algorithm>

int main() {
    mpk3::payload_t payload;
    while (payload.from_is(std::cin)) {
        YAML::Node pgm(payload);

        std::string const & pgm_name = pgm["name"].as<std::string>();
        unsigned short const pgm_id = pgm["number"].as<unsigned short>();
        std::cout << "received program #" << pgm_id << " : " << pgm_name << "\n";

        std::string fname = std::to_string(pgm_id) + "_" + pgm_name + ".yml";
        std::string const badchars = " :@#$%!?*/\\\"'`";
        std::for_each(badchars.begin(), badchars.end(),
                [&](const char c){
                    std::replace(fname.begin(), fname.end(), c, '_');
                });

        std::cout << "emitting " << fname << "\n";
        std::ofstream ofs(fname, std::ofstream::out);
        ofs << "---\n" << pgm;
        ofs.close();
    }
    return 0;
}

