#include "mpk3_yaml.hpp"
#include "mpk3_types.hpp"
#include "yaml-cpp/yaml.h"
#include <cassert>
#include <cstring>

namespace mpk3 {

    class mapper_t {
        std::vector<std::string> const map;
        public:
        mapper_t(std::vector<std::string> const & map_) : map(map_) {}
        size_t to_num(std::string const & str) const {
            size_t i=0;
            while(i<map.size() && map[i] != str) ++i;
            assert(i<map.size() && "not found in map");
            return i;
        }
        std::string const & to_str(short const num) const {
            return map.at(num);
        }
    };

    static mapper_t const aftertouch_map({
            "off"
            , "channel"
            , "polyphonic"
            });

    YAML::Node payload_t::as_yaml() const {
        YAML::Node pgm;
        pgm["name"] = name;
        pgm["number"] = static_cast<short>(number);
        {
            YAML::Node kb = pgm["keybed"];
            kb["channel"] = static_cast<short>(key_channel + 1);
            kb["octave"] = static_cast<short>(key_octave - 4);
            kb["transpose"] = static_cast<short>(transpose - 12);
        }
        {
            YAML::Node pad_node = pgm["pad"];
            pad_node["channel"] = static_cast<short>(pad_channel + 1);
            pad_node["aftertouch"] = aftertouch_map.to_str(pad_aftertouch);
        }
        pgm["arppeggiator"] = YAML::Node(arp);
        pgm["joystick"] = YAML::Node(joystick);
        {
            YAML::Node pad_vector = pgm["pads"];
            for (size_t i_pad=0; i_pad<16; i_pad++) {
                pad_vector.push_back(pad[i_pad]);
            }
        }
        {
            YAML::Node knob_vector = pgm["knobs"];
            for (size_t i_knob=0; i_knob<8; i_knob++) {
                knob_vector.push_back(knob[i_knob]);
            }
        }
        return pgm;
    }

    bool payload_t::from_yaml(YAML::Node const & pgm) {
        std::string const & temp = pgm["name"].as<std::string>();
        strncpy(name, temp.c_str(), 16);
        number = pgm["number"].as<short>();
        key_channel = pgm["keybed"]["channel"].as<short>() - 1;
        key_octave = pgm["keybed"]["octave"].as<short>() + 4;
        transpose = pgm["keybed"]["transpose"].as<short>() + 12;
        pad_channel = pgm["pad"]["channel"].as<short>() - 1;
        pad_aftertouch = aftertouch_map.to_num(pgm["pad"]["aftertouch"].as<std::string>());
        arp = pgm["arppeggiator"].as<arp_t>();
        joystick = pgm["joystick"].as<stick_t>();
        for (size_t i_pad=0; i_pad<16; i_pad++) {
            pad[i_pad] = pgm["pads"][i_pad].as<pad_t>();
        }
        for (size_t i_knob=0; i_knob<8; i_knob++) {
            knob[i_knob] = pgm["knobs"][i_knob].as<knob_t>();
        }
        return true;
    }

    static mapper_t const arp_mode_map({
            "up"
            , "down"
            , "exclusive"
            , "inclusive"
            , "ordered"
            , "random"
            });
    static mapper_t const arp_div_map({
            "1/4"
            , "1/4T"
            , "1/8"
            , "1/8T"
            , "1/16"
            , "1/16T"
            , "1/32"
            , "1/32T"
            });
    static mapper_t const arp_clock_source({
            "internal"
            , "external"
            });

    YAML::Node arp_t::as_yaml() const {
        YAML::Node arp;
        arp["switch"] = on ? "on" : "off";
        arp["mode"] = arp_mode_map.to_str(mode);
        arp["time_division"] = arp_div_map.to_str(division);
        arp["latch"] = latch_on ? "on" : "off";
        arp["octave"] = static_cast<short>(octave + 1);
        arp["swing"] = static_cast<short>(swing + 50);
        arp["clock_source"] = arp_clock_source.to_str(clock_source);
        arp["tempo_taps"] = static_cast<short>(taps);
        arp["bpm"] = static_cast<short>(bpm);
        return arp;
    }

    bool arp_t::from_yaml(YAML::Node const & arp) {
        on = arp["switch"].as<std::string>() == "on" ? 0x7f : 0x00;
        mode = arp_mode_map.to_num(arp["mode"].as<std::string>());
        division = arp_div_map.to_num(arp["time_division"].as<std::string>());
        latch_on = arp["latch"].as<std::string>() == "on" ? 0x01 : 0x00;
        octave = arp["octave"].as<short>() - 1;
        swing = arp["swing"].as<short>() - 50;
        clock_source = arp_clock_source.to_num(arp["clock_source"].as<std::string>());
        taps = arp["tempo_taps"].as<short>();
        bpm = arp["bpm"].as<short>();
        return true;
    }

    static mapper_t const stick_mode_map({
            "pitchbend"
            , "single"
            , "dual"
            });

    YAML::Node stick_t::as_yaml() const {
        YAML::Node stick;
        stick["horizontal_mode"] = stick_mode_map.to_str(hmode);
        stick["controller_left"] = static_cast<short>(cc_left);
        stick["controller_right"] = static_cast<short>(cc_right);
        stick["vertical_mode"] = stick_mode_map.to_str(vmode);
        stick["controller_down"] = static_cast<short>(cc_down);
        stick["controller_up"] = static_cast<short>(cc_up);
        return stick;
    }

    bool stick_t::from_yaml(YAML::Node const & stick) {
        hmode = stick_mode_map.to_num(stick["horizontal_mode"].as<std::string>());
        cc_left = stick["controller_left"].as<short>();
        cc_right = stick["controller_right"].as<short>();
        vmode = stick_mode_map.to_num(stick["vertical_mode"].as<std::string>());
        cc_down = stick["controller_down"].as<short>();
        cc_up = stick["controller_up"].as<short>();
        return true;
    }

    YAML::Node pad_t::as_yaml() const {
        YAML::Node pad;
        pad["note"] = static_cast<short>(note);
        pad["controller"] = static_cast<short>(cc);
        pad["program"] = static_cast<short>(pc);
        return pad;
    }

    bool pad_t::from_yaml(YAML::Node const & pad) {
        note = pad["note"].as<short>();
        cc = pad["controller"].as<short>();
        pc = pad["program"].as<short>();
        return true;
    }

    static mapper_t const knob_mode_map({
            "absolute"
            , "relative"
            });

    YAML::Node knob_t::as_yaml() const {
        YAML::Node knob;
        knob["name"] = name;
        knob["controller"] = static_cast<short>(cc);
        knob["mode"] = knob_mode_map.to_str(mode);
        knob["min"] = static_cast<short>(min);
        knob["max"] = static_cast<short>(max);
        return knob;
    }

    bool knob_t::from_yaml(YAML::Node const & knob) {
        std::string const & temp = knob["name"].as<std::string>();
        strncpy(name, temp.c_str(), 16);
        cc = knob["controller"].as<short>();
        mode = knob_mode_map.to_num(knob["mode"].as<std::string>());
        min = knob["min"].as<short>();
        max = knob["max"].as<short>();
        return true;
    }
}
