#include "mpk3_io.hpp"
#include "mpk3_types.hpp"
#include <iomanip>


std::istream & operator>>(std::istream & is, mpk3::token_t & v) {
    // consume white space
    {
        char next = is.peek();
        while (next == ' ' || next == '\n') {
            is.ignore(1);
            next = is.peek();
        }
    }
    char buf[3];
    is.read(buf, 2);
    v = mpk3::token_t(buf);
    return is;
}


mpk3::token_t mpk3::get_token(std::istream & is) {
    mpk3::token_t buf;
    is >> buf;
    return std::move(buf);
}


std::istream & operator>>(std::istream & is, mpk3::uint14_t & v) {
    v.hi = std::stoul(mpk3::get_token(is), nullptr, 16);
    v.lo = std::stoul(mpk3::get_token(is), nullptr, 16);
    return is;
}


namespace mpk3 {

    bool payload_t::from_is(std::istream & in) {
        // sync
        while (!in.eof() && get_token(in) != "F0");          //SysEx Start
        if (in.eof()) return false;
        { // check and discard
            token_t token;
            in >> token; if (token != "47" || in.eof()) return false; //Vendor: Akai
            in >> token; if (token != "00" || in.eof()) return false; //Read from Device
            in >> token; if (token != "49" || in.eof()) return false; //Product: MPKmini MK3
            in >> token; if (token != "67" || in.eof()) return false; //Data Receive
            uint14_t payload_len;
            in >> payload_len; if (payload_len != 246 || in.eof()) return false;
        }
        for (uint8_t i=0; i<246; i++) {
            reinterpret_cast<uint8_t*>(this)[i] = std::stoul(get_token(in), nullptr, 16);
            if (in.eof()) return false;
        }
        { // check and discard
            token_t token;
            in >> token; if (token != "F7" || in.eof()) return false; //SysEx End
        }
        return true;
    }

}

std::ostream & operator<<(std::ostream & os, mpk3::uint14_t const & v) {
    os << std::setfill ('0') << std::setw(2) << std::hex << static_cast<short>(v.hi) << " " << static_cast<short>(v.lo);
    return os;
}

std::ostream & operator<<(std::ostream & os, mpk3::payload_t const & pl) {
    char const * payload = reinterpret_cast<char const*>(&pl);
    mpk3::uint14_t payload_len = sizeof(pl);
    os << "F0 47 7F 49 64 " << payload_len;
    for (size_t i=0; i<payload_len; i++) {
        os << " " << std::setfill ('0') << std::setw(2) << std::hex << static_cast<short>(payload[i]);
    }
    os << " F7";
    return os;
}
