#ifndef MPK3_TYPES_HPP
#define MPK3_TYPES_HPP

#include "mpk3_io.hpp"
#include "yaml-cpp/yaml.h"
#include <cstdint>
#include <iostream>

namespace mpk3 {

    class uint14_t {
        uint8_t hi;
        uint8_t lo;

        public:
        uint14_t() { *this = 0; }
        uint14_t(uint16_t const rhs) { *this = rhs; }
        operator uint16_t() const { return (hi<<7) + (lo&0x7f); }
        uint14_t& operator =(uint16_t const & rhs) {
            hi = ((rhs&0x3f80)>>7);
            lo = (rhs&0x7f);
            return *this;
        }
        friend std::istream & ::operator>>(std::istream &, uint14_t &);
        friend std::ostream & ::operator<<(std::ostream &, mpk3::uint14_t const &);
    };

    class arp_t {
        uint8_t on;
        uint8_t mode;
        uint8_t division;
        uint8_t clock_source;
        uint8_t latch_on;
        uint8_t swing;
        uint8_t taps;
        uint14_t bpm;
        uint8_t octave;

        public:
        bool from_yaml(YAML::Node const & arp);
        YAML::Node as_yaml() const;
    };

    class stick_t {
        uint8_t hmode;
        uint8_t cc_left;
        uint8_t cc_right;
        uint8_t vmode;
        uint8_t cc_down;
        uint8_t cc_up;

        public:
        bool from_yaml(YAML::Node const & stick);
        YAML::Node as_yaml() const;
    };

    class pad_t {
        uint8_t note;
        uint8_t pc;
        uint8_t cc;

        public:
        bool from_yaml(YAML::Node const & pad);
        YAML::Node as_yaml() const;
    };

    class knob_t {
        uint8_t mode;
        uint8_t cc;
        uint8_t min;
        uint8_t max;
        char name[16];

        public:
        bool from_yaml(YAML::Node const & knob);
        YAML::Node as_yaml() const;
    };

    class payload_t {
        uint8_t number;
        char name[16];
        uint8_t pad_channel;
        uint8_t pad_aftertouch;
        uint8_t key_channel;
        uint8_t key_octave;
        arp_t arp;
        stick_t joystick;
        pad_t pad[16];
        knob_t knob[8];
        uint8_t transpose;

        public:
        bool from_is(std::istream & in);
        bool from_yaml(YAML::Node const & root);
        friend std::ostream & ::operator<<(std::ostream &, mpk3::payload_t const &);
        YAML::Node as_yaml() const;
    };

}

#endif
