#ifndef MPK3_YAML_HPP
#define MPK3_YAML_HPP

#include "yaml-cpp/yaml.h"
#include "mpk3_types.hpp"

namespace YAML {
    template<>
        struct convert<mpk3::payload_t> {
            static Node encode(mpk3::payload_t const & payload) {
                return payload.as_yaml();
            }
            static bool decode(Node const & node, mpk3::payload_t & payload) {
                return payload.from_yaml(node);
            }
        };

    template<>
        struct convert<mpk3::arp_t> {
            static Node encode(mpk3::arp_t const & arp) {
                return arp.as_yaml();
            }
            static bool decode(Node const & node, mpk3::arp_t & arp) {
                return arp.from_yaml(node);
            }
        };

    template<>
        struct convert<mpk3::stick_t> {
            static Node encode(mpk3::stick_t const & stick) {
                return stick.as_yaml();
            }
            static bool decode(Node const & node, mpk3::stick_t & stick) {
                return stick.from_yaml(node);
            }
        };

    template<>
        struct convert<mpk3::pad_t> {
            static Node encode(mpk3::pad_t const & pad) {
                return pad.as_yaml();
            }
            static bool decode(Node const & node, mpk3::pad_t & pad) {
                return pad.from_yaml(node);
            }
        };

    template<>
        struct convert<mpk3::knob_t> {
            static Node encode(mpk3::knob_t const & knob) {
                return knob.as_yaml();
            }
            static bool decode(Node const & node, mpk3::knob_t & knob) {
                return knob.from_yaml(node);
            }
        };
}

#endif
