#ifndef MPK3_IO_HPP
#define MPK3_IO_HPP

#include <string>
#include <iostream>

namespace mpk3 {
    using token_t = std::string;
    class uint14_t;
    class payload_t;
    token_t get_token(std::istream &);
}

std::istream & operator>>(std::istream &, mpk3::token_t &);
std::istream & operator>>(std::istream &, mpk3::uint14_t &);
std::ostream & operator<<(std::ostream &, mpk3::uint14_t const &);
std::ostream & operator<<(std::ostream &, mpk3::payload_t const &);

#endif

